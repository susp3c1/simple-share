const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const multer = require("multer");

const app = express();

function randomString(length) {
  let result = "";
  for (var i = 0; i < length; i++) {
    result += Math.floor(Math.random() * 10);
  }
  return result;
}

// Allow acces from everywhre
app.use(cors({ origin: "*" }));
app.use(bodyParser.urlencoded({ extended: true }));
// Set public Dir that is Exposed
app.use("/download/", express.static("public/"));

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public");
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "_" + randomString(10) + "_" + file.originalname);
  }
});

const upload = multer({ storage: storage });

app.post("/upload/:key", upload.single("file"), (req, res, next) => {
  const key = req.params.key;
  const file = req.file;
  if (!file && key === process.env.KEY) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  }
  res.send("/download/" + file.filename);
});

// Set Port
const port = process.env.MODE === "LIVE" ? 8010 : 8091;
app.listen(port, () => {
  console.log("App listening on port " + port);
});
